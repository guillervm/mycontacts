package com.guillervm.mycontacts

import com.guillervm.mycontacts.ioc.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class MyContactsApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out MyContactsApplication> {
        return DaggerAppComponent.builder().create(this)
    }

}