package com.guillervm.mycontacts.common

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.guillervm.mycontacts.data.model.ErrorData
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel : ViewModel() {

    val loader: MutableLiveData<Boolean> = MutableLiveData()
    val error: MutableLiveData<ErrorData?> = MutableLiveData()
    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}