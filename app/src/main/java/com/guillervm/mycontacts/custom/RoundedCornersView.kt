package com.guillervm.mycontacts.custom

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import com.guillervm.mycontacts.R
import com.guillervm.mycontacts.util.DisplayUtils

class RoundedCornersView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val defaultRadius = DisplayUtils.dpToPixels(4.0f)
    private var radiusValues: FloatArray
    private var paint: Paint
    private var path: Path
    private var helperRectF = RectF()
    private var gradientStartColor: Int? = null
    private var gradientEndColor: Int? = null

    init {
        var isGradient = false
        var color = ContextCompat.getColor(context, android.R.color.transparent)
        var radiusCornerTopLeft = defaultRadius
        var radiusCornerTopRight = defaultRadius
        var radiusCornerBottomRight = defaultRadius
        var radiusCornerBottomLeft = defaultRadius

        if (attrs != null) {
            val a = context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.RoundedCornersView, 0, 0
            )
            try {
                if (a.hasValue(R.styleable.RoundedCornersView_backgroundGradientStartColor) && a.hasValue(R.styleable.RoundedCornersView_backgroundGradientEndColor)) {
                    isGradient = true
                    gradientStartColor = a.getColor(
                        R.styleable
                            .RoundedCornersView_backgroundGradientStartColor, ContextCompat.getColor(
                            context, android.R.color.transparent
                        )
                    )
                    gradientEndColor = a.getColor(
                        R.styleable.RoundedCornersView_backgroundGradientEndColor,
                        ContextCompat.getColor(context, android.R.color.transparent)
                    )
                } else {
                    color = a.getColor(
                        R.styleable.RoundedCornersView_backgroundColor, ContextCompat
                            .getColor(context, android.R.color.transparent)
                    )
                }
                if (!a.hasValue(R.styleable.RoundedCornersView_radiusCorners)) {
                    radiusCornerTopLeft = a.getDimension(
                        R.styleable.RoundedCornersView_radiusCornerTopLeft,
                        defaultRadius
                    )
                    radiusCornerTopRight = a.getDimension(
                        R.styleable.RoundedCornersView_radiusCornerTopRight,
                        defaultRadius
                    )
                    radiusCornerBottomRight = a.getDimension(
                        R.styleable.RoundedCornersView_radiusCornerBottomRight, defaultRadius
                    )
                    radiusCornerBottomLeft = a.getDimension(
                        R.styleable.RoundedCornersView_radiusCornerBottomLeft, defaultRadius
                    )
                } else {
                    radiusCornerBottomLeft = a.getDimension(
                        R.styleable.RoundedCornersView_radiusCorners,
                        defaultRadius
                    )
                    radiusCornerBottomRight = radiusCornerBottomLeft
                    radiusCornerTopRight = radiusCornerBottomRight
                    radiusCornerTopLeft = radiusCornerTopRight
                }
            } catch (e: Exception) {
                //
            } finally {
                a.recycle()
            }
        }

        radiusValues = floatArrayOf(
            radiusCornerTopLeft,
            radiusCornerTopLeft,
            radiusCornerTopRight,
            radiusCornerTopRight,
            radiusCornerBottomRight,
            radiusCornerBottomRight,
            radiusCornerBottomLeft,
            radiusCornerBottomLeft
        )
        paint = Paint()
        paint.isAntiAlias = true
        paint.style = Paint.Style.FILL
        if (isGradient && gradientStartColor != null && gradientEndColor != null) {
            paint.shader = LinearGradient(
                0f, 0f, width.toFloat(), height.toFloat(), gradientStartColor!!,
                gradientEndColor!!, Shader.TileMode.MIRROR
            )
        } else {
            paint.color = color
        }
        path = Path()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if (paint.shader != null && gradientStartColor != null && gradientEndColor != null) {
            paint.shader = LinearGradient(
                0f, 0f, width.toFloat(), height.toFloat(), gradientStartColor!!, gradientEndColor!!,
                Shader.TileMode.MIRROR
            )
        }
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        helperRectF.set(canvas.clipBounds)
        path.rewind()
        path.addRoundRect(helperRectF, radiusValues, Path.Direction.CW)
        canvas.drawPath(path, paint)
    }

    fun setColor(@ColorInt color: Int) {
        gradientStartColor = null
        gradientEndColor = null
        paint.shader = null
        paint.color = color
        invalidate()
    }

    fun setGradientColors(@ColorInt startColor: Int, @ColorInt endColor: Int) {
        gradientStartColor = startColor
        gradientEndColor = endColor
        paint.shader = LinearGradient(
            0f, 0f, width.toFloat(), height.toFloat(), startColor, endColor,
            Shader.TileMode.MIRROR
        )
        invalidate()
    }

    fun setCornersRadius(radius: Float) {
        setCornersRadius(radius, radius, radius, radius)
    }

    fun setCornersRadius(
        radiusTopLeft: Float,
        radiusTopRight: Float,
        radiusBottomLeft: Float,
        radiusBottomRight: Float
    ) {
        radiusValues = floatArrayOf(
            radiusTopLeft,
            radiusTopLeft,
            radiusTopRight,
            radiusTopRight,
            radiusBottomLeft,
            radiusBottomLeft,
            radiusBottomRight,
            radiusBottomRight
        )
        invalidate()
    }

}