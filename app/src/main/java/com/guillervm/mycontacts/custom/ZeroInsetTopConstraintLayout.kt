package com.guillervm.mycontacts.custom

import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.WindowInsets
import androidx.constraintlayout.widget.ConstraintLayout

class ZeroInsetTopConstraintLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    override fun onApplyWindowInsets(insets: WindowInsets?): WindowInsets {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && insets != null) {
            setPadding(
                insets.systemWindowInsetLeft, 0, insets.systemWindowInsetRight,
                insets.systemWindowInsetBottom
            )
            return insets.replaceSystemWindowInsets(0, insets.systemWindowInsetTop, 0, 0)
        }
        return super.onApplyWindowInsets(insets)
    }

}