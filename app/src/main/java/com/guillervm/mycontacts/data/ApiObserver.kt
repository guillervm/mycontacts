package com.guillervm.mycontacts.data

import com.guillervm.mycontacts.data.model.ErrorData
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class ApiObserver<T> constructor(private val compositeDisposable: CompositeDisposable) : Observer<T> {

    override fun onSubscribe(d: Disposable) {
        compositeDisposable.add(d)
    }

    override fun onComplete() {
        //
    }

    override fun onNext(t: T) {
        onData(t)
        onSuccess()
    }

    override fun onError(e: Throwable) {
        onError(ErrorData(throwable = e, message = e.localizedMessage))
    }

    abstract fun onData(data: T)

    abstract fun onSuccess()

    abstract fun onError(e: ErrorData)
}