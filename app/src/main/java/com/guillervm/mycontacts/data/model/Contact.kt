package com.guillervm.mycontacts.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Contact(
    var id: Int,
    var name: String = "",
    var starred: Boolean = false,
    var photoUriThumbnail: String? = null,
    var photoUriFullSize: String? = null,
    var phones: MutableList<Phone> = mutableListOf(),
    var emails: MutableList<Email> = mutableListOf(),
    var organizations: MutableList<Organization> = mutableListOf(),
    var postalAddresses: MutableList<PostalAddress> = mutableListOf(),
    var events: MutableList<Event> = mutableListOf(),
    var websites: MutableList<String> = mutableListOf(),
    var notes: MutableList<String> = mutableListOf()
) : Parcelable {

    override fun toString(): String {
        val builder = StringBuilder(name)
        builder.append(" ($id)")
        if (starred) {
            builder.append(" ⭑")
        }
        builder.append("\n")
        builder.append(" Has thumbnail? ${photoUriThumbnail != null}\n")
        builder.append(" Has image? ${photoUriFullSize != null}\n")
        builder.append(" Phones (${phones.size})\n")
        for (phone in phones) {
            builder.append("   ${phone.number}  ${phone.normalizedNumber}  (${phone.type} - ${phone.label})\n")
        }
        builder.append(" Emails (${emails.size})\n")
        for (email in emails) {
            builder.append("   ${email.address}  (${email.type} - ${email.label})\n")
        }
        builder.append(" Organizations (${organizations.size})\n")
        for (organization in organizations) {
            builder.append("   ${organization.title} at ${organization.company} (${organization.officeLocation})\n")
        }
        builder.append(" Postal Addresses (${postalAddresses.size})\n")
        for (postalAddress in postalAddresses) {
            builder.append("   ${postalAddress.formattedAddress}  (${postalAddress.type} - ${postalAddress.label})\n")
        }
        builder.append(" Events (${events.size})\n")
        for (event in events) {
            builder.append("   ${event.startDate}  (${event.type} - ${event.label})\n")
        }
        builder.append(" Websites (${websites.size})\n")
        for (website in websites) {
            builder.append("   $website\n")
        }
        builder.append(" Notes (${websites.size})\n")
        for (note in notes) {
            builder.append("   $note\n")
        }

        return builder.toString()
    }

}