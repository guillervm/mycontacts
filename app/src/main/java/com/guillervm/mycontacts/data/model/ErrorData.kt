package com.guillervm.mycontacts.data.model

class ErrorData(var message: String = "", var errorCode: String = "", var throwable: Throwable? = null)