package com.guillervm.mycontacts.data.model

import android.content.Context
import android.os.Parcelable
import android.provider.ContactsContract
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Event(var startDate: String, var type: Int, var label: String? = null) : Parcelable {

    fun getFormattedLabel(context: Context): String {
        return ContactsContract.CommonDataKinds.Event.getTypeLabel(context.resources, type, label).toString()
    }

}