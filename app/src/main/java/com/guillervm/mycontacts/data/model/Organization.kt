package com.guillervm.mycontacts.data.model

import android.os.Parcelable
import android.text.TextUtils
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Organization(var company: String? = null, var title: String? = null, var officeLocation: String? = null) :
    Parcelable {

    fun getPrimaryText(): String? {
        return if (!TextUtils.isEmpty(title)) {
            title!!
        } else if (!TextUtils.isEmpty(company)) {
            company!!
        } else if (!TextUtils.isEmpty(officeLocation)) {
            officeLocation!!
        } else {
            null
        }
    }

    fun getSecondaryText(): String? {
        return if (!TextUtils.isEmpty(title)) {
            val builder = StringBuilder()
            if (!TextUtils.isEmpty(company)) {
                builder.append(company)
            }
            if (!TextUtils.isEmpty(officeLocation)) {
                if (!builder.isEmpty()) {
                    builder.append(" ($officeLocation)")
                } else {
                    builder.append(officeLocation)
                }
            }
            if (!builder.isEmpty()) builder.toString() else null
        } else if (!TextUtils.isEmpty(company)) {
            if (!TextUtils.isEmpty(officeLocation)) {
                officeLocation
            } else {
                null
            }
        } else {
            null
        }
    }

}