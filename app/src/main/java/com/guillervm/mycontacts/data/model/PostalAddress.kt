package com.guillervm.mycontacts.data.model

import android.content.Context
import android.os.Parcelable
import android.provider.ContactsContract
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PostalAddress(var formattedAddress: String, var type: Int, var label: String? = null) : Parcelable {

    fun getFormattedLabel(context: Context): String {
        return ContactsContract.CommonDataKinds.StructuredPostal.getTypeLabel(context.resources, type, label).toString()
    }

}