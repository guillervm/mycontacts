package com.guillervm.mycontacts.data.repository

import android.content.Context
import android.provider.ContactsContract
import android.text.TextUtils
import com.guillervm.mycontacts.data.model.*
import io.reactivex.Observable
import javax.inject.Inject

class ContactsRepository @Inject constructor(var context: Context) {

    fun getContacts(): Observable<List<Contact>> {
        return Observable.create {

            // Sets the columns to retrieve for the user profile
            val projection = arrayOf(
                ContactsContract.Data.CONTACT_ID,
                ContactsContract.Data.DISPLAY_NAME_PRIMARY,
                ContactsContract.Data.STARRED,
                ContactsContract.Data.PHOTO_URI,
                ContactsContract.Data.PHOTO_THUMBNAIL_URI,
                ContactsContract.Data.MIMETYPE,
                ContactsContract.Data.DATA1,
                ContactsContract.Data.DATA2,
                ContactsContract.Data.DATA3,
                ContactsContract.Data.DATA4,
                ContactsContract.Data.DATA5,
                ContactsContract.Data.DATA6,
                ContactsContract.Data.DATA7,
                ContactsContract.Data.DATA8,
                ContactsContract.Data.DATA9,
                ContactsContract.Data.DATA10,
                ContactsContract.Data.DATA11,
                ContactsContract.Data.DATA12,
                ContactsContract.Data.DATA13,
                ContactsContract.Data.DATA14,
                ContactsContract.Data.DATA15
            )

            // Filter the mimetypes we would like to retrieve
            val selection = "${ContactsContract.Data.MIMETYPE} IN (" +
                    "'${ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE}', " +
                    "'${ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE}', " +
                    "'${ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE}', " +
                    "'${ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE}', " +
                    "'${ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE}', " +
                    "'${ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE}', " +
                    "'${ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE}')"

            // Sort by name ascending and case insensitive
            val sortOrder = "${ContactsContract.Data.DISPLAY_NAME_PRIMARY} COLLATE NOCASE ASC"

            // Retrieves the profile from the Contacts Provider
            val cursor = context.contentResolver.query(
                ContactsContract.Data.CONTENT_URI,
                projection,
                selection,
                null,
                sortOrder
            )

            val contacts = mutableListOf<Contact>()

            if (cursor != null) {
                cursor.moveToFirst()

                val columnIndexContactId = cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID)
                val columnIndexDisplayNamePrimary =
                    cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME_PRIMARY)
                val columnIndexStarred = cursor.getColumnIndex(ContactsContract.Data.STARRED)
                val columnIndexPhotoThumbnailUri =
                    cursor.getColumnIndex(ContactsContract.Data.PHOTO_THUMBNAIL_URI)
                val columnIndexPhotoUri = cursor.getColumnIndex(ContactsContract.Data.PHOTO_URI)
                val columnIndexMimeType = cursor.getColumnIndex(ContactsContract.Data.MIMETYPE)
                val columnIndexesDataSet = mapOf(
                    Pair(ContactsContract.Data.DATA1, cursor.getColumnIndex(ContactsContract.Data.DATA1)),
                    Pair(ContactsContract.Data.DATA2, cursor.getColumnIndex(ContactsContract.Data.DATA2)),
                    Pair(ContactsContract.Data.DATA3, cursor.getColumnIndex(ContactsContract.Data.DATA3)),
                    Pair(ContactsContract.Data.DATA4, cursor.getColumnIndex(ContactsContract.Data.DATA4)),
                    Pair(ContactsContract.Data.DATA5, cursor.getColumnIndex(ContactsContract.Data.DATA5)),
                    Pair(ContactsContract.Data.DATA6, cursor.getColumnIndex(ContactsContract.Data.DATA6)),
                    Pair(ContactsContract.Data.DATA7, cursor.getColumnIndex(ContactsContract.Data.DATA7)),
                    Pair(ContactsContract.Data.DATA8, cursor.getColumnIndex(ContactsContract.Data.DATA8)),
                    Pair(ContactsContract.Data.DATA9, cursor.getColumnIndex(ContactsContract.Data.DATA9)),
                    Pair(ContactsContract.Data.DATA10, cursor.getColumnIndex(ContactsContract.Data.DATA10)),
                    Pair(ContactsContract.Data.DATA11, cursor.getColumnIndex(ContactsContract.Data.DATA11)),
                    Pair(ContactsContract.Data.DATA12, cursor.getColumnIndex(ContactsContract.Data.DATA12)),
                    Pair(ContactsContract.Data.DATA13, cursor.getColumnIndex(ContactsContract.Data.DATA13)),
                    Pair(ContactsContract.Data.DATA14, cursor.getColumnIndex(ContactsContract.Data.DATA14)),
                    Pair(ContactsContract.Data.DATA15, cursor.getColumnIndex(ContactsContract.Data.DATA15)) // Blob
                )
                var contact: Contact
                var matchContact: Contact?
                var mimeType: String
                var id: Int
                while (!cursor.isAfterLast) {
                    id = cursor.getInt(columnIndexContactId)
                    matchContact = contacts.firstOrNull { match -> match.id == id }
                    contact = matchContact ?: Contact(cursor.getInt(columnIndexContactId))
                    contact.name = cursor.getString(columnIndexDisplayNamePrimary) ?: ""
                    contact.starred = cursor.getInt(columnIndexStarred) != 0
                    contact.photoUriThumbnail = cursor.getString(columnIndexPhotoThumbnailUri)
                    contact.photoUriFullSize = cursor.getString(columnIndexPhotoUri)
                    mimeType = cursor.getString(columnIndexMimeType)
                    when (mimeType) {
                        ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE -> {
                            val addressColumn = columnIndexesDataSet[ContactsContract.CommonDataKinds.Email.ADDRESS]
                            val typeColumn = columnIndexesDataSet[ContactsContract.CommonDataKinds.Email.TYPE]
                            val labelColumn = columnIndexesDataSet[ContactsContract.CommonDataKinds.Email.DISPLAY_NAME]
                            if (addressColumn != null && typeColumn != null && labelColumn != null) {
                                contact.emails.add(
                                    Email(
                                        cursor.getString(addressColumn),
                                        cursor.getInt(typeColumn),
                                        cursor.getString(labelColumn)
                                    )
                                )
                            }
                        }
                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE -> {
                            val numberColumn = columnIndexesDataSet[ContactsContract.CommonDataKinds.Phone.NUMBER]
                            val normalizedNumberColumn =
                                columnIndexesDataSet[ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER]
                            val typeColumn = columnIndexesDataSet[ContactsContract.CommonDataKinds.Phone.TYPE]
                            val labelColumn = columnIndexesDataSet[ContactsContract.CommonDataKinds.Phone.LABEL]
                            if (numberColumn != null && normalizedNumberColumn != null && typeColumn != null && labelColumn != null) {
                                contact.phones.add(
                                    Phone(
                                        cursor.getString(numberColumn),
                                        cursor.getString(normalizedNumberColumn),
                                        cursor.getInt(typeColumn),
                                        cursor.getString(labelColumn)
                                    )
                                )
                            }
                        }
                        ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE -> {
                            val companyColumn =
                                columnIndexesDataSet[ContactsContract.CommonDataKinds.Organization.COMPANY]
                            val titleColumn = columnIndexesDataSet[ContactsContract.CommonDataKinds.Organization.TITLE]
                            val officeLocationColumn =
                                columnIndexesDataSet[ContactsContract.CommonDataKinds.Organization.OFFICE_LOCATION]
                            if (companyColumn != null && titleColumn != null && officeLocationColumn != null) {
                                contact.organizations.add(
                                    Organization(
                                        cursor.getString(companyColumn),
                                        cursor.getString(titleColumn),
                                        cursor.getString(officeLocationColumn)
                                    )
                                )
                            }
                        }
                        ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE -> {
                            val formattedAddressColumn =
                                columnIndexesDataSet[ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS]
                            val typeColumn =
                                columnIndexesDataSet[ContactsContract.CommonDataKinds.StructuredPostal.TYPE]
                            val labelColumn =
                                columnIndexesDataSet[ContactsContract.CommonDataKinds.StructuredPostal.LABEL]
                            if (formattedAddressColumn != null && typeColumn != null && labelColumn != null) {
                                contact.postalAddresses.add(
                                    PostalAddress(
                                        cursor.getString(formattedAddressColumn),
                                        cursor.getInt(typeColumn),
                                        cursor.getString(labelColumn)
                                    )
                                )
                            }
                        }
                        ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE -> {
                            val startDateColumn =
                                columnIndexesDataSet[ContactsContract.CommonDataKinds.Event.START_DATE]
                            val typeColumn = columnIndexesDataSet[ContactsContract.CommonDataKinds.Event.TYPE]
                            val labelColumn = columnIndexesDataSet[ContactsContract.CommonDataKinds.Event.LABEL]
                            if (startDateColumn != null && typeColumn != null && labelColumn != null) {
                                contact.events.add(
                                    Event(
                                        cursor.getString(startDateColumn),
                                        cursor.getInt(typeColumn),
                                        cursor.getString(labelColumn)
                                    )
                                )
                            }
                        }
                        ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE -> {
                            val urlColumn = columnIndexesDataSet[ContactsContract.CommonDataKinds.Website.URL]
                            if (urlColumn != null) {
                                contact.websites.add(cursor.getString(urlColumn))
                            }
                        }
                        ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE -> {
                            val noteColumn = columnIndexesDataSet[ContactsContract.CommonDataKinds.Note.NOTE]
                            if (noteColumn != null) {
                                val note = cursor.getString(noteColumn)
                                if (!TextUtils.isEmpty(note)) {
                                    contact.notes.add(note)
                                }
                            }
                        }
                    }
                    if (matchContact == null) {
                        contacts.add(contact)
                    }
                    cursor.moveToNext()
                }
                cursor.close()
            }

            it.onNext(contacts)
            it.onComplete()
        }
    }

}