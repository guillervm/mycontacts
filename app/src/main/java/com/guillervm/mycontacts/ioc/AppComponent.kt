package com.guillervm.mycontacts.ioc

import com.guillervm.mycontacts.MyContactsApplication
import com.guillervm.mycontacts.ioc.annotation.ApplicationScope
import com.guillervm.mycontacts.ioc.module.ActivityModule
import com.guillervm.mycontacts.ioc.module.AppModule
import com.guillervm.mycontacts.ioc.module.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@ApplicationScope
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ViewModelModule::class,
        ActivityModule::class
    ]
)
internal interface AppComponent : AndroidInjector<MyContactsApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<MyContactsApplication>()

}