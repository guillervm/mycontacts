package com.guillervm.mycontacts.ioc.module

import com.guillervm.mycontacts.ioc.annotation.ActivityScope
import com.guillervm.mycontacts.view.details.DetailsActivity
import com.guillervm.mycontacts.view.details.DetailsActivityModule
import com.guillervm.mycontacts.view.main.MainActivity
import com.guillervm.mycontacts.view.main.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun bindMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [DetailsActivityModule::class])
    abstract fun bindDetailsActivity(): DetailsActivity

}