package com.guillervm.mycontacts.ioc.module

import android.content.Context
import com.guillervm.mycontacts.MyContactsApplication
import com.guillervm.mycontacts.ioc.annotation.ApplicationScope
import dagger.Binds
import dagger.Module

@Module
abstract class AppModule {

    @Binds
    @ApplicationScope
    abstract fun bindContext(application: MyContactsApplication): Context

}