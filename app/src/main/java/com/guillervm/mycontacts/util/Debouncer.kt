package com.guillervm.mycontacts.util

import java.util.*

class Debouncer(private val debounceTime: Long = 500L) {

    private var lastAction = 0L

    fun action(): Boolean {
        if (Calendar.getInstance().timeInMillis > lastAction + debounceTime) {
            lastAction = Calendar.getInstance().timeInMillis
            return true
        }
        return false
    }
}