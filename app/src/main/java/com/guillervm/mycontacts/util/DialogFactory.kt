package com.guillervm.mycontacts.util

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.guillervm.mycontacts.R

class DialogFactory {

    companion object {

        fun displayGenericErrorDialog(context: Context) {
            AlertDialog.Builder(context)
                .setMessage(R.string.generic_error_text)
                .setPositiveButton(R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }
                .create()
                .show()
        }

    }

}