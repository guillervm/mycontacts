package com.guillervm.mycontacts.view.details

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.request.RequestOptions
import com.guillervm.mycontacts.R
import com.guillervm.mycontacts.common.BaseActivity
import com.guillervm.mycontacts.common.GlideApp
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : BaseActivity() {

    private lateinit var detailsViewModel: DetailsViewModel
    private lateinit var detailsAdapter: DetailsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        detailsViewModel = ViewModelProviders.of(this, viewModelFactory).get(DetailsViewModel::class.java)
        observeLoader(arrayOf(detailsViewModel))
        observeError(arrayOf(detailsViewModel))

        detailsViewModel.contact.observe(this, Observer {
            GlideApp
                .with(this)
                .load(it.photoUriFullSize)
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.placeholder_contact).into(iv_photo)
            tv_name.text = it.name
            detailsAdapter.setContactDetails(it, this)
        })

        detailsAdapter = DetailsAdapter()

        rv_details.layoutManager = LinearLayoutManager(this).apply { orientation = RecyclerView.VERTICAL }
        rv_details.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            val scalePhoto = 0.8f
            val scaleName = 0.4f
            var totalY = 0
            var lastProgress = -1.0f
            val finalHeight: Float
            val scrollableSpace: Float

            init {
                finalHeight = resources.getDimension(R.dimen.details_photo_size) * (1.0f - scalePhoto) +
                        resources.getDimension(R.dimen.details_name_height) * (1.0f - scaleName)
                scrollableSpace = resources.getDimension(R.dimen.details_photo_size) +
                        resources.getDimension(R.dimen.details_name_height) - finalHeight
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                totalY += dy
                val progress = Math.min(totalY / scrollableSpace, 1.0f)
                if (progress != lastProgress) {
                    lastProgress = progress
                    iv_photo.scaleX = 1.0f - scalePhoto * progress
                    iv_photo.scaleY = iv_photo.scaleX
                    iv_photo.translationY =  -(iv_photo.height - iv_photo.height * iv_photo.scaleY) / 2
                    rcv_photo.scaleX = iv_photo.scaleX
                    rcv_photo.scaleY = iv_photo.scaleY
                    rcv_photo.translationY = iv_photo.translationY
                    tv_name.scaleX = 1.0f - scaleName * progress
                    tv_name.scaleY = tv_name.scaleX
                    tv_name.translationY = iv_photo.translationY * 2 - (tv_name.height - tv_name.height * tv_name.scaleY) / 2
                    top_background.alpha = Math.max((progress * progress - 0.5f) / 0.5f, 0.0f)
                    top_background.translationY = tv_name.translationY - (tv_name.height - tv_name.height * tv_name.scaleY) / 2
                }
            }
        })
        rv_details.adapter = detailsAdapter

        detailsViewModel.setDataFromBundle(intent.extras)
    }

    companion object {
        const val EXTRA_CONTACT = "EXTRA_CONTACT"
    }

}