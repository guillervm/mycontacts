package com.guillervm.mycontacts.view.details

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.guillervm.mycontacts.R
import com.guillervm.mycontacts.data.model.Contact
import com.guillervm.mycontacts.view.details.viewholder.ItemElementViewHolder
import com.guillervm.mycontacts.view.details.viewholder.ItemHeaderViewHolder

class DetailsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val items = mutableListOf<Data>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_ELEMENT -> ItemElementViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_details_element,
                    parent,
                    false
                )
            )
            else -> ItemHeaderViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_details_header,
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            VIEW_TYPE_ELEMENT -> {
                val data = items[position] as ElementData
                (holder as ItemElementViewHolder).bind(
                    data.primaryText,
                    data.secondaryText,
                    data.isTop,
                    data.isBottom
                )
            }
            else -> {
                val data = items[position] as HeaderData
                (holder as ItemHeaderViewHolder).bind(data.icon)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].type
    }

    fun setContactDetails(contact: Contact, helperContext: Context) {
        items.clear()

        if (contact.phones.isNotEmpty()) {
            items.add(HeaderData(R.drawable.ic_phone))
            for (i in 0 until contact.phones.size) {
                items.add(
                    ElementData(
                        contact.phones[i].number,
                        contact.phones[i].getFormattedLabel(helperContext),
                        i == 0,
                        i == contact.phones.size - 1
                    )
                )
            }
        }

        if (contact.emails.isNotEmpty()) {
            items.add(HeaderData(R.drawable.ic_email))
            for (i in 0 until contact.emails.size) {
                items.add(
                    ElementData(
                        contact.emails[i].address,
                        contact.emails[i].getFormattedLabel(helperContext),
                        i == 0,
                        i == contact.emails.size - 1
                    )
                )
            }
        }
        if (contact.organizations.isNotEmpty()) {
            items.add(HeaderData(R.drawable.ic_organization))
            for (i in 0 until contact.organizations.size) {
                items.add(
                    ElementData(
                        contact.organizations[i].getPrimaryText(),
                        contact.organizations[i].getSecondaryText(),
                        i == 0,
                        i == contact.organizations.size - 1
                    )
                )
            }
        }
        if (contact.websites.isNotEmpty()) {
            items.add(HeaderData(R.drawable.ic_website))
            for (i in 0 until contact.websites.size) {
                items.add(
                    ElementData(
                        contact.websites[i],
                        null,
                        i == 0,
                        i == contact.websites.size - 1
                    )
                )
            }
        }
        if (contact.postalAddresses.isNotEmpty()) {
            items.add(HeaderData(R.drawable.ic_address))
            for (i in 0 until contact.postalAddresses.size) {
                items.add(
                    ElementData(
                        contact.postalAddresses[i].formattedAddress,
                        contact.postalAddresses[i].getFormattedLabel(helperContext),
                        i == 0,
                        i == contact.postalAddresses.size - 1
                    )
                )
            }
        }
        if (contact.events.isNotEmpty()) {
            items.add(HeaderData(R.drawable.ic_event))
            for (i in 0 until contact.events.size) {
                items.add(
                    ElementData(
                        contact.events[i].startDate,
                        contact.events[i].getFormattedLabel(helperContext),
                        i == 0,
                        i == contact.events.size - 1
                    )
                )
            }
        }
        if (contact.notes.isNotEmpty()) {
            items.add(HeaderData(R.drawable.ic_note))
            for (i in 0 until contact.notes.size) {
                items.add(
                    ElementData(
                        contact.notes[i],
                        null,
                        i == 0,
                        i == contact.notes.size - 1
                    )
                )
            }
        }

        notifyDataSetChanged()
    }

    abstract class Data(val type: Int)

    data class HeaderData(val icon: Int) : Data(VIEW_TYPE_HEADER)

    data class ElementData(
        val primaryText: String?,
        val secondaryText: String?,
        val isTop: Boolean,
        val isBottom: Boolean
    ) : Data(VIEW_TYPE_ELEMENT)

    companion object {
        const val VIEW_TYPE_HEADER = 0
        const val VIEW_TYPE_ELEMENT = 1
    }

}