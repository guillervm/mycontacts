package com.guillervm.mycontacts.view.details

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import com.guillervm.mycontacts.common.BaseViewModel
import com.guillervm.mycontacts.data.model.Contact
import javax.inject.Inject

class DetailsViewModel @Inject constructor() : BaseViewModel() {

    var contact = MutableLiveData<Contact>()

    fun setDataFromBundle(bundle: Bundle?) {
        contact.value = bundle?.getParcelable(DetailsActivity.EXTRA_CONTACT)
    }

}