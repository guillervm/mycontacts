package com.guillervm.mycontacts.view.details.viewholder

import android.text.TextUtils
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.guillervm.mycontacts.util.DisplayUtils
import kotlinx.android.synthetic.main.item_details_element.view.*

class ItemElementViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    fun bind(primaryText: String?, secondaryText: String?, isTop: Boolean, isBottom: Boolean) {
        view.tv_primary.text = primaryText
        view.tv_secondary.text = secondaryText
        view.tv_secondary.visibility = if (!TextUtils.isEmpty(secondaryText)) View.VISIBLE else View.GONE
        view.rcv_background.setCornersRadius(
            if (isTop) radius else 0.0f,
            if (isTop) radius else 0.0f,
            if (isBottom) radius else 0.0f,
            if (isBottom) radius else 0.0f
        )
        view.divider.visibility = if (isBottom) View.GONE else View.VISIBLE
    }

    companion object {
        var radius = DisplayUtils.dpToPixels(8.0f)
    }

}