package com.guillervm.mycontacts.view.details.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_details_header.view.*

class ItemHeaderViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    fun bind(icon: Int) {
        view.iv_icon.setImageResource(icon)
    }

}