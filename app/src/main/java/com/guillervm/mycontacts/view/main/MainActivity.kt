package com.guillervm.mycontacts.view.main

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.guillervm.mycontacts.R
import com.guillervm.mycontacts.common.BaseActivity
import com.guillervm.mycontacts.util.Debouncer
import com.guillervm.mycontacts.view.details.DetailsActivity
import com.guillervm.mycontacts.view.main.viewholder.MainAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    private lateinit var mainViewModel: MainViewModel
    private lateinit var mainAdapter: MainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        observeLoader(arrayOf(mainViewModel))
        observeError(arrayOf(mainViewModel))

        val adapterClickDebouncer = Debouncer()
        mainAdapter = MainAdapter {
            if (adapterClickDebouncer.action()) {
                val intent = Intent(this, DetailsActivity::class.java)
                intent.putExtra(DetailsActivity.EXTRA_CONTACT, it)
                startActivity(intent)
            }
        }

        rv_contacts.layoutManager = LinearLayoutManager(this).apply { orientation = RecyclerView.VERTICAL }
        rv_contacts.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            var totalY = 0
            var lastProgress = -1.0f
            val toolbarHeight: Int
            val scrollableSpace: Float

            init {
                val ta = obtainStyledAttributes(intArrayOf(R.attr.actionBarSize))
                toolbarHeight = ta.getDimensionPixelSize(0, -1)
                scrollableSpace = resources.getDimension(R.dimen.main_header_height) - toolbarHeight
                ta.recycle()
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                totalY += dy
                val progress = Math.min(totalY / scrollableSpace, 1.0f)
                if (progress != lastProgress) {
                    lastProgress = progress
                    tv_title.scaleX = 1.0f - 0.4f * progress * progress
                    tv_title.scaleY = tv_title.scaleX
                    tv_title.translationY = (toolbarHeight - tv_title.height) / 2.0f * progress
                    title_background.alpha =
                            Math.pow(Math.max(((progress - 0.7f) / 0.3f), 0.0f).toDouble(), 2.0).toFloat()
                }
            }
        })
        rv_contacts.adapter = mainAdapter

        checkPermission()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_READ_CONTACTS -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) permissionGranted() else permissionDenied()
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun checkPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_CONTACTS),
                PERMISSION_REQUEST_READ_CONTACTS
            )
        } else {
            permissionGranted()
        }
    }

    private fun permissionGranted() {
        mainViewModel.contacts.observe(this, Observer {
            mainAdapter.updateContacts(it, this)
        })
        mainViewModel.getContacts()
    }

    private fun permissionDenied() {

    }

    companion object {
        const val PERMISSION_REQUEST_READ_CONTACTS = 0
    }

}