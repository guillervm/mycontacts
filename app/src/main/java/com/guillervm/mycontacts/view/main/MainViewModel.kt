package com.guillervm.mycontacts.view.main

import androidx.lifecycle.MutableLiveData
import com.guillervm.mycontacts.common.BaseViewModel
import com.guillervm.mycontacts.data.ApiObserver
import com.guillervm.mycontacts.data.model.Contact
import com.guillervm.mycontacts.data.model.ErrorData
import com.guillervm.mycontacts.data.repository.ContactsRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainViewModel @Inject constructor(private val contactsRepository: ContactsRepository) : BaseViewModel() {

    var contacts = MutableLiveData<List<Contact>>()

    fun getContacts() {
        loader.value = true
        contactsRepository.getContacts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ApiObserver<List<Contact>>(compositeDisposable) {
                override fun onData(data: List<Contact>) {
                    loader.value = false
                    contacts.value = data
                }

                override fun onSuccess() {
                    //
                }

                override fun onError(e: ErrorData) {
                    loader.value = false
                    error.value = e
                }
            })
    }

}