package com.guillervm.mycontacts.view.main.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.request.RequestOptions
import com.guillervm.mycontacts.R
import com.guillervm.mycontacts.common.GlideApp
import com.guillervm.mycontacts.util.DisplayUtils
import kotlinx.android.synthetic.main.item_main_contact.view.*

class ItemContactViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    fun bind(photoUri: String?, name: String, isTop: Boolean, isBottom: Boolean) {
        GlideApp.with(view)
            .load(photoUri)
            .apply(RequestOptions.circleCropTransform())
            .placeholder(R.drawable.placeholder_contact)
            .into(view.iv_photo)
        view.tv_name.text = name
        view.rcv_background.setCornersRadius(
            if (isTop) radius else 0.0f,
            if (isTop) radius else 0.0f,
            if (isBottom) radius else 0.0f,
            if (isBottom) radius else 0.0f
        )
        view.divider.visibility = if (isBottom) View.GONE else View.VISIBLE
    }

    companion object {
        var radius = DisplayUtils.dpToPixels(8.0f)
    }

}