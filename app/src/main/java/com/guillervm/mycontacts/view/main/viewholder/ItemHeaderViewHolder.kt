package com.guillervm.mycontacts.view.main.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_main_header.view.*

class ItemHeaderViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    fun bind(text: String) {
        view.tv_text.text = text
    }

}