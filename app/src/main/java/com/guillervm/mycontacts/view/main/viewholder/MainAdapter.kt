package com.guillervm.mycontacts.view.main.viewholder

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.guillervm.mycontacts.R
import com.guillervm.mycontacts.data.model.Contact

class MainAdapter(val onClick: (Contact) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val items = mutableListOf<Data>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_CONTACT -> ItemContactViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_main_contact,
                    parent,
                    false
                )
            )
            else -> ItemHeaderViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_main_header,
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            VIEW_TYPE_CONTACT -> {
                val data = items[position] as ContactData
                (holder as ItemContactViewHolder).bind(
                    data.contact.photoUriThumbnail,
                    data.contact.name,
                    data.isTop,
                    data.isBottom
                )
                holder.view.setOnClickListener { onClick(data.contact) }
            }
            else -> {
                val data = items[position] as HeaderData
                (holder as ItemHeaderViewHolder).bind(data.text)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].type
    }

    fun updateContacts(contacts: List<Contact>, helperContext: Context) {
        val previousSize = items.size
        items.clear()

        val groupedContacts = contacts.groupBy { it.starred }
        var group = groupedContacts[true]
        if (group != null) {
            items.add(HeaderData(helperContext.getString(R.string.starred)))
            for (i in 0 until group.size) {
                items.add(ContactData(group[i], i == 0, i == group.size - 1))
            }
        }

        group = groupedContacts[false]
        if (group != null) {
            val groupedByLetterContacts = group.groupBy { it.name.substring(0, 1).toUpperCase() }
            for ((firstLetter, contactsLetter) in groupedByLetterContacts) {
                items.add(HeaderData(firstLetter))
                for (i in 0 until contactsLetter.size) {
                    items.add(ContactData(contactsLetter[i], i == 0, i == contactsLetter.size - 1))
                }
            }
        }

        val newSize = items.size

        if (previousSize != 0) {
            if (newSize != previousSize) {
                if (newSize > previousSize) {
                    notifyItemRangeChanged(0, previousSize)
                    notifyItemRangeInserted(previousSize, newSize)
                } else {
                    notifyItemRangeChanged(0, newSize)
                    notifyItemRangeRemoved(newSize, previousSize)
                }
            } else {
                notifyItemRangeChanged(0, newSize)
            }
        } else {
            notifyItemRangeInserted(0, newSize)
        }
    }

    abstract class Data(val type: Int)

    data class HeaderData(val text: String) : Data(VIEW_TYPE_HEADER)

    data class ContactData(val contact: Contact, val isTop: Boolean, val isBottom: Boolean) :
        Data(VIEW_TYPE_CONTACT)

    companion object {
        const val VIEW_TYPE_HEADER = 0
        const val VIEW_TYPE_CONTACT = 1
    }
}