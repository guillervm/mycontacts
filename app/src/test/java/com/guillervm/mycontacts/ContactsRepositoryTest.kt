package com.guillervm.mycontacts

import android.content.ContentProvider
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.MatrixCursor
import android.net.Uri
import android.provider.ContactsContract
import androidx.test.core.app.ApplicationProvider
import com.guillervm.mycontacts.data.repository.ContactsRepository
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.shadows.ShadowContentResolver


@RunWith(RobolectricTestRunner::class)
class ContactsRepositoryTest {

    private val contactsRepository: ContactsRepository =
        ContactsRepository(ApplicationProvider.getApplicationContext<Context>())

    @Test
    fun getContacts() {
        val matrixCursor = MatrixCursor(DataFactory.getGetContactsProjection())
        for (row in DataFactory.getGetContactsCursorRows()) {
            matrixCursor.addRow(row)
        }

        val mockProvider = ContactsMockContentProvider()
        mockProvider.addQueryResult(ContactsContract.Data.CONTENT_URI, matrixCursor)

        ShadowContentResolver.registerProviderInternal(ContactsContract.AUTHORITY, mockProvider)

        val expectedResult = DataFactory.getGetContactsList()

        val testObserver = contactsRepository.getContacts().test()
        testObserver.awaitTerminalEvent()

        testObserver
            .assertNoErrors()
            .assertValue {
                it == expectedResult
            }
    }

    class ContactsMockContentProvider : ContentProvider() {

        private val expectedResults = HashMap<Uri, Cursor>()

        fun addQueryResult(uriIn: Uri, expectedResult: Cursor) {
            expectedResults[uriIn] = expectedResult
        }

        override fun query(
            uri: Uri,
            projection: Array<String>,
            selection: String?,
            selectionArgs: Array<String>?,
            sortOrder: String?
        ): Cursor? {
            return expectedResults[uri]
        }

        override fun insert(uri: Uri, values: ContentValues?): Uri? {
            return null
        }

        override fun onCreate(): Boolean {
            return false
        }

        override fun update(uri: Uri, values: ContentValues?, selection: String?, selectionArgs: Array<String>?): Int {
            return -1
        }

        override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
            return -1
        }

        override fun getType(uri: Uri): String? {
            return "type"
        }

    }
}
