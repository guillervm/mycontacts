package com.guillervm.mycontacts

import android.provider.ContactsContract
import com.guillervm.mycontacts.data.model.*

class DataFactory {

    companion object {

        fun getGetContactsProjection(): Array<String> {
            return arrayOf(
                ContactsContract.Data.CONTACT_ID,
                ContactsContract.Data.DISPLAY_NAME_PRIMARY,
                ContactsContract.Data.STARRED,
                ContactsContract.Data.PHOTO_URI,
                ContactsContract.Data.PHOTO_THUMBNAIL_URI,
                ContactsContract.Data.MIMETYPE,
                ContactsContract.Data.DATA1,
                ContactsContract.Data.DATA2,
                ContactsContract.Data.DATA3,
                ContactsContract.Data.DATA4,
                ContactsContract.Data.DATA5,
                ContactsContract.Data.DATA6,
                ContactsContract.Data.DATA7,
                ContactsContract.Data.DATA8,
                ContactsContract.Data.DATA9,
                ContactsContract.Data.DATA10,
                ContactsContract.Data.DATA11,
                ContactsContract.Data.DATA12,
                ContactsContract.Data.DATA13,
                ContactsContract.Data.DATA14,
                ContactsContract.Data.DATA15
            )
        }

        fun getGetContactsCursorRows(): Array<Array<String?>> {
            return arrayOf(
                arrayOf(
                    "0",
                    "Alice",
                    "1",
                    "someOtherUri",
                    "someUri",
                    ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE,
                    "+44 7777 777777",
                    ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE.toString(),
                    null,
                    "+447777777777",
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ),
                arrayOf(
                    "0",
                    "Alice",
                    "1",
                    "someOtherUri",
                    "someUri",
                    ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE,
                    "alice@homeemail.com",
                    ContactsContract.CommonDataKinds.Email.TYPE_HOME.toString(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ),
                arrayOf(
                    "0",
                    "Alice",
                    "1",
                    "someOtherUri",
                    "someUri",
                    ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE,
                    "alice@organization.com",
                    ContactsContract.CommonDataKinds.Email.TYPE_WORK.toString(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ),
                arrayOf(
                    "0",
                    "Alice",
                    "1",
                    "someOtherUri",
                    "someUri",
                    ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE,
                    "Organization Ltd.",
                    null,
                    null,
                    "Chief Testing Officer",
                    null,
                    null,
                    null,
                    null,
                    "London",
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ),
                arrayOf(
                    "0",
                    "Alice",
                    "1",
                    "someOtherUri",
                    "someUri",
                    ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE,
                    "13 Random St, A12 B34, London, UK",
                    ContactsContract.CommonDataKinds.StructuredPostal.TYPE_WORK.toString(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ),
                arrayOf(
                    "0",
                    "Alice",
                    "1",
                    "someOtherUri",
                    "someUri",
                    ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE,
                    "1993-01-15",
                    ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY.toString(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ),
                arrayOf(
                    "0",
                    "Alice",
                    "1",
                    "someOtherUri",
                    "someUri",
                    ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE,
                    "organization.com",
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ),
                arrayOf(
                    "0",
                    "Alice",
                    "1",
                    "someOtherUri",
                    "someUri",
                    ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE,
                    "\"Quality is never an accident. It is always the result of intelligent effort.\" - John Ruskin",
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ),
                arrayOf(
                    "1",
                    "Bob",
                    "0",
                    null,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE,
                    "+44 7777 666666",
                    ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE.toString(),
                    null,
                    "+447777666666",
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ),
                arrayOf(
                    "1",
                    "Bob",
                    "0",
                    null,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE,
                    "+44 7777 555555",
                    ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE.toString(),
                    null,
                    "+447777555555",
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                )
            )
        }

        fun getGetContactsList(): List<Contact> {
            val contacts = mutableListOf<Contact>()

            contacts.add(
                Contact(
                    0,
                    "Alice",
                    true,
                    "someUri",
                    "someOtherUri",
                    mutableListOf(
                        Phone(
                            "+44 7777 777777",
                            "+447777777777",
                            ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE
                        )
                    ),
                    mutableListOf(
                        Email(
                            "alice@homeemail.com",
                            ContactsContract.CommonDataKinds.Email.TYPE_HOME
                        ),
                        Email(
                            "alice@organization.com",
                            ContactsContract.CommonDataKinds.Email.TYPE_WORK
                        )
                    ),
                    mutableListOf(
                        Organization(
                            "Organization Ltd.",
                            "Chief Testing Officer",
                            "London"
                        )
                    ),
                    mutableListOf(
                        PostalAddress(
                            "13 Random St, A12 B34, London, UK",
                            ContactsContract.CommonDataKinds.StructuredPostal.TYPE_WORK
                        )
                    ),
                    mutableListOf(
                        Event(
                            "1993-01-15",
                            ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY
                        )
                    ),
                    mutableListOf(
                        "organization.com"
                    ),
                    mutableListOf(
                        "\"Quality is never an accident. It is always the result of intelligent effort.\" - John Ruskin"
                    )
                )
            )

            contacts.add(
                Contact(
                    1,
                    "Bob",
                    false,
                    phones = mutableListOf(
                        Phone(
                            "+44 7777 666666",
                            "+447777666666",
                            ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE
                        ),
                        Phone(
                            "+44 7777 555555",
                            "+447777555555",
                            ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE
                        )
                    )
                )
            )

            return contacts
        }

    }

}