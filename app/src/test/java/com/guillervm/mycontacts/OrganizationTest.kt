package com.guillervm.mycontacts

import com.guillervm.mycontacts.data.model.Organization
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class OrganizationTest {

    private val company = "Testing Ltd"
    private val title = "Chief Testing Officer"
    private val officeLocation = "London"
    private val organizationEmpty = Organization()
    private val organizationOnlyCompany = Organization(company = company)
    private val organizationOnlyTitle = Organization(title = title)
    private val organizationOnlyOfficeLocation = Organization(officeLocation = officeLocation)
    private val organizationOnlyCompanyTitle = Organization(company = company, title = title)
    private val organizationOnlyCompanyOfficeLocation = Organization(company = company, officeLocation = officeLocation)
    private val organizationOnlyTitleOfficeLocation = Organization(title = title, officeLocation = officeLocation)
    private val organizationFull = Organization(company, title, officeLocation)

    @Test
    fun getPrimaryText() {
        assertEquals(organizationEmpty.getPrimaryText(), null)
        assertEquals(organizationOnlyCompany.getPrimaryText(), company)
        assertEquals(organizationOnlyTitle.getPrimaryText(), title)
        assertEquals(organizationOnlyOfficeLocation.getPrimaryText(), officeLocation)
        assertEquals(organizationOnlyCompanyTitle.getPrimaryText(), title)
        assertEquals(organizationOnlyCompanyOfficeLocation.getPrimaryText(), company)
        assertEquals(organizationOnlyTitleOfficeLocation.getPrimaryText(), title)
        assertEquals(organizationFull.getPrimaryText(), title)
    }

    @Test
    fun getSecondaryText() {
        assertEquals(organizationEmpty.getSecondaryText(), null)
        assertEquals(organizationOnlyCompany.getSecondaryText(), null)
        assertEquals(organizationOnlyTitle.getSecondaryText(), null)
        assertEquals(organizationOnlyOfficeLocation.getSecondaryText(), null)
        assertEquals(organizationOnlyCompanyTitle.getSecondaryText(), company)
        assertEquals(organizationOnlyCompanyOfficeLocation.getSecondaryText(), officeLocation)
        assertEquals(organizationOnlyTitleOfficeLocation.getSecondaryText(), officeLocation)
        assertEquals(organizationFull.getSecondaryText(), "$company ($officeLocation)")
    }

}